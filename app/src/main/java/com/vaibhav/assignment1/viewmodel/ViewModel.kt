package com.vaibhav.assignment1.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vaibhav.assignment1.model.User
import kotlinx.coroutines.launch

class ViewModel : ViewModel() {

    var usersList = mutableListOf<User>()
    var usersListData = MutableLiveData<MutableList<User>>()

    fun getAllUsers() = usersListData.value

    fun addUser(user: User) {
        viewModelScope.launch {
            usersList.add(user)
            usersListData.value = usersList
        }
    }

    fun deleteUser(position: Int) {
        usersList.removeAt(position)
        usersListData.value = usersList
    }
}