package com.vaibhav.assignment1.adapter

import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vaibhav.assignment1.R
import com.vaibhav.assignment1.ViewHolder
import com.vaibhav.assignment1.model.User

class RecyclerViewAdapter(private var itemList: List<User>) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        itemList[position].name.also { holder.textView.text = it }
        holder.cardView.setOnClickListener {
            val dialog = AlertDialog.Builder(holder.itemView.context)
            dialog.setTitle("Information")
            dialog.setMessage(itemList[position].name + "\n" + itemList[position].email + "\n" + itemList[position].age.toString() + "\n")
            dialog.show()
        }
    }

    override fun getItemCount(): Int = itemList.size
}