package com.vaibhav.assignment1

import android.app.AlertDialog
import android.app.Dialog
import android.view.View
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView

class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val textView: TextView = itemView.findViewById(R.id.name_title)
    val cardView: MaterialCardView = itemView.findViewById(R.id.cardView)
}