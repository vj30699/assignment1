package com.vaibhav.assignment1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vaibhav.assignment1.adapter.RecyclerViewAdapter
import com.vaibhav.assignment1.model.User
import com.vaibhav.assignment1.viewmodel.ViewModel
import com.vaibhav.assignment1.viewmodel.ViewModelFactory

class MainActivity : AppCompatActivity(), LifecycleObserver {

    lateinit var viewModel: ViewModel
    lateinit var name: EditText
    lateinit var email: EditText
    lateinit var age: EditText
    lateinit var addButton: Button
    lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initialSetup()
        recyclerViewSetup()

    }

    fun initialSetup() {
        viewModel = ViewModelProvider(this, ViewModelFactory())[ViewModel::class.java]
        name = findViewById<EditText>(R.id.name)
        email = findViewById<EditText>(R.id.email)
        age = findViewById<EditText>(R.id.age)
        addButton = findViewById<Button>(R.id.add)
        recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
    }

    private fun recyclerViewSetup() {
        var adapter = viewModel.getAllUsers()?.let { RecyclerViewAdapter(it) }

        viewModel.usersListData.observe(this, {
            adapter = it?.let { RecyclerViewAdapter(it) }
            recyclerView
        })

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        addButton.setOnClickListener {
            if (name.text.isEmpty() || age.text.isEmpty() || email.text.isEmpty()) {
                Toast.makeText(this, "All fields are mandatory!", Toast.LENGTH_LONG).show()
            } else {
                val user = User(
                    name = name.text.toString(),
                    email = email.text.toString(),
                    age = age.text.toString().toInt()
                )
                viewModel.addUser(user)
            }
        }

        val itemTouchHelperCallback =
            object :
                ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    viewModel.deleteUser(viewHolder.adapterPosition)
                    Toast.makeText(
                        this@MainActivity,
                        getString(R.string.node_deleted),
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }

        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(recyclerView)

    }

}