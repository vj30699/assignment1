package com.vaibhav.assignment1.model

data class User(
    val name: String,
    val email: String,
    val age: Int
)
